import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import App from '../imports/ui/App.jsx';
import HomePage from '../imports/ui/HomePage.jsx';
import NotFoundPage from '../imports/ui/NotFoundPage.jsx';
import Chat from '../imports/ui/Chat.jsx';

import { Router,Route, IndexRoute, browserHistory } from 'react-router';

Meteor.startup(function() {

  ReactDOM.render((
    <Router history={browserHistory}>
      <Route path="/" component={App}>
      	<IndexRoute component={HomePage}/>        
        <Route path="chat" component={Chat} />
        <Route path="*" component={NotFoundPage} />        
      </Route>
    </Router>
  ), document.getElementById('render-target'));
});