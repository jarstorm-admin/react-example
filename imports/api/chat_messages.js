import { Mongo } from 'meteor/mongo';
 
export const ChatMessages = new Mongo.Collection('chat_message');

if (Meteor.isServer) {
  Meteor.publish('chatMessages', function chatMessagesPublication() {
    return ChatMessages.find({}, {sort: {createdAt: 1}});
  });
}
