import React, { Component, PropTypes } from 'react';
 import {Link } from 'react-router';
 import { ChatMessages } from '../api/chat_messages.js';
import ChatMessage from './ChatMessage.jsx';
import { createContainer } from 'meteor/react-meteor-data';

class Chat extends Component {

  renderMessages() {

return this.props.messages.map((message) => (
          <ChatMessage key={message._id} date={message.createdAt} user={message.createdBy} message={message.message} />
        ));
    
  }

handleSubmit(event) {
    event.preventDefault();
 
    const node = ReactDOM.findDOMNode(this.refs.textInput);

    const text = node.value.trim();

    ChatMessages.insert({
      message: text,
      createdAt: new Date(),
      createdBy: Meteor.user().emails[0].address
    });

    // Clear form
    node.value = '';
  }

  componentDidUpdate() {
  scrollToBottom();  
  }

   render() {

    return (
    <div>
    <div className="panel panel-default">
    
    <div className="panel-heading">Chat messages</div>
  <div className="panel-body" id="chat_messages_panel">
      <ul>
          {this.renderMessages()}
        </ul>
  </div>
</div>

  
        <form onSubmit={this.handleSubmit.bind(this)}>
        <input type="text" ref="textInput"/>
        </form>
        </div>
      )
      };

}
Chat.propTypes = {
  messages: PropTypes.array.isRequired,
};

 
 export default createContainer(() => {
 Meteor.subscribe('chatMessages');
  return {
    messages: ChatMessages.find({}).fetch(),
  };
}, Chat);


function scrollToBottom() {
    const div = document.getElementById('chat_messages_panel');
    console.log(div);
    div.scrollTop = div.scrollHeight + 10;
  }