import React, { Component, PropTypes } from 'react';

import AppMenu from './AppMenu.jsx';

// App component - represents the whole app
export default class App extends Component {

  render() {
    return (
      <div className="container">
        <AppMenu />
        {this.props.children}
      </div>
    );
  }
}
