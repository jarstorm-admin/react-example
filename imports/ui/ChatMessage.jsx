import React, { Component } from 'react';
 import {Link } from 'react-router';
 
export default class ChatMessage extends Component {

	render() {
		var messageClass;
		if (Meteor.user() && Meteor.user().emails[0].address === this.props.user) {
			messageClass = "user_message";
		} 

		return (
			<p className={messageClass}>{this.props.date.toString()} - {this.props.user} - {this.props.message}</p>
		)
	}
}
 